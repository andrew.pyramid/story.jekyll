---
Title: visa.country.md 
---



During COVID-19 and cheap of living

Dany:

    Turkey
    Albania
    Ukraine
    Serbia


Digital Nomad Living Costs

    Geogery
    Hungry
    Chile
    Czech Republic $1500.
    Thailand $
    Taiwan ?
    Estonia  (Digital Nomad Visa)
    Vietanian
    Barbados
    Indonesia
    Georgia (prove you have $1k)

    Philipine $500.



中秋节后，哪些国家将会重开边境？
如题，哪些国家将在最近重开边境？
2019年的那一场大瘟疫，至今让多数国家关掉了正常的边境，非常态成为常态，何时结束？
2021年中秋。

--------
下面是不定时收集的一些资料，不一定准确。

如果是英语的资讯就引用原文，只在有时间的时候翻译一下，请有能力的朋友在评论区帮助翻译。
也许可以做为反贼出逃的路线参考，但是请不要以此为权威资讯，会不时更新，也请大家补充。

# 泰国， 2021年 10月

Thailand is set to reopen more of its popular tourist destinations starting next month, betting that a higher local inoculation rate can help draw more foreign visitors and revive an economy battered by the pandemic.

The reopening of capital city Bangkok and Chiang Mai as well as beach resorts Pattaya, Cha-Am and Hua Hin from Oct. 1 will be modeled after an initiative to bring back vaccinated tourists to Phuket, tourism ministry officials said. More destinations, including Chiang Rai, Koh Chang and Koh Kood, may fully reopen for visitors from mid-October, with travel bubbles planned with neighboring countries next year, they said.

The push to end pandemic-induced restrictions on tourists is part of Thailand’s new “living with Covid-19” strategy amid a raging outbreak that seeks to keep infections to a level that doesn’t overwhelm its health-care system. Countries from Singapore to Indonesia and the Philippines are tweaking policies to deal with the endemic nature of the outbreak to protect livelihoods and save their economies from extending a slump.

Sources: Health Ministry, Bloomberg

For Thai Prime Minister Prayuth Chan-Ocha, the revival of tourism is a key priority as it contributed about a fifth of the nation’s gross domestic product in 2019, the year before the pandemic.

One of the key criteria for Thai tourist spots to waive the quarantine requirement for fully-inoculated tourists is 70% local vaccination rate. The resort island of Phuket became the first Thai province to meet that target and reopened in July, with the program extending to nearby islands and beach communities last month.

Vaccination Coverage
Thai provinces aim to reach 70% inoculation rate before their reopenings

Sources: Health Ministry, Bloomberg

Data as of Aug. 31; Pattaya is in Chonburi, Cha-Am in Phetchaburi and Hua Hin in Prachuap Khiri Khan

Thailand has said that its so-called Phuket Sandbox is a success, and the program would be emulated across the country. More than 26,000 vaccinated travelers used the Sandbox in the first two months, generating 1.6 billion baht ($49 million) in tourism revenue, Phuket officials said. Less than 1% of the visitors tested positive for the virus even though local infections increased.


# 澳大利亚 Australia

这两天刚才提了一下重开边境的事情：
https://youtu.be/5QLDDJOSGhY


2021年9月底， 
-------------------

abcmoney.com.uk 估计：

When is tourism likely to resume?

Currently, Australia’s travel restrictions are due to continue until at least the end of 2021. Nevertheless, it is expected that the country won’t resume tourism until early 2022.

At the moment it is not possible to travel to Australia at all. 
Normally tourists are allowed to enter the country with a visa or visa waiver such as the Australian ETA.
However, these authorizations are currently suspended due to the imposed health restrictions.
They should be continued once tourism recommences. 


# New Zealand

预计明年初（2022）。目前继续 zero tolerance strategy,  边境关闭。

https://youtu.be/lhWhrkn_IDY 

# 中国边境从越南到阿富汗和更西边的斯坦国都在严控中，

据说：中越边境拉了铁丝网，部分可能带电。缅甸边境更有地雷。 但是这两个地方都没有朋友亲自证实过。

从新疆出境应该尤其困难。

## 老挝

据说可以申请商务 VISA

### 2021-8-13 

近日，老挝政府出台一份关于出入境问题的新的入境文件，大致内容如下：
①做入境批文必须先交300美金医疗保险和GPS手环佩戴费；
②劳动部严格审查公司企业的用工指标，不符合规定和超额的企业不能邀请外籍人员入境老挝；
③移民局对公司和企业备案，哪个公司邀请的外籍人员到老挝后从事违法犯罪活动的，被抓获后公司承担连带责任，罚款3000美金每人。
在此之前的入境政策为：

    1.申请办理入境老挝批文和签证（没有押金和保险要求）
    2.购买入境老挝机票
    3.出行前3天做好核酸检测
    4.预定入境的隔离酒店
    5.下飞机乘坐政府车到达酒店隔离14天（准备5万老币或5美金现金做车费）

（ 入境检查好自己的资料是否带齐全：护照+入境签证+入境批文打印件+酒店预订单+核酸检测证明。建议提前3小时到达机场，凭所有资料换取登机牌。）

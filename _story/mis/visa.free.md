---
Title: visa.free.md 
---



整理一下容易签证的国家，
请大家帮忙为跑路反贼准备一点签证知识，
有不对的地方请指正，有过期的资料请刷新。

到达自由签证的三个国家：



护照无需签证的国家：

 Bahamas	 [33]	3 months[34]		√

 Haiti	 [120]	3 months		X

 Saint Kitts and Nevis	 [223]	3 months		X

 Seychelles	 [238]	3 months[239]		√

 Fiji	 [102]	4 months		√

 Saint Lucia	 [224][225]	6 weeks		X

 Uzbekistan	 [289]	10 days	
Must arrive by air. Otherwise an electronic visa is available.[290]
X

 Oman	 [205]	14 days	* Chinese national are also eligible for single / multiple entry electronic visa valid for 30 days.	X

 Dominica	 [86]	21 days		X

 Iran	 [128][129]	21 days[130]		X

 Antigua and Barbuda	 [19]	30 days		X

 Barbados	 [37]	30 days		√

 Belarus	 [38]	30 days	
Visa free for 30 days, for a maximum total stay of 90 days within any year period.[39][40]
Holders of passports for public affairs do not require a visa for up to 30 days.[41]
√

 Grenada	 [113]	30 days[114]	
Beginning on December 1, 2020, all travellers to Grenada will be required to complete an online application in order to receive a Pure Safe Travel Authorization Certificate to enter Grenada.[115]
√

 Indonesia	 [127]	30 days		X

 Jamaica	 [138]	30 days		X

 Micronesia	 [179]	30 days		X

 Qatar	 [218]	30 days		√

 Serbia	 [236]	30 days[237]	
Holders of passports for public affairs do not require a visa up to 90 days within a six-month period.
√

 Suriname	 [254]	30 days	
Visa liberalization agreement between China and Suriname was ratified on 1 May 2021.[255]
COVID-19 entry restrictions still apply for foreign citizens entering Suriname.
√

 Tonga	 [266]	30 days[267]		√

 Ukraine	 [277]	30 days	
Temporary Visa free regime from the 1st of April, 2021 to the 30th of September, 2021.[278]
Ukrainian authorities intend to sign reciprocal visa-free agreement with China in the next five years.[279]
X
 United Arab Emirates	 [280]	30 days		√

 Vanuatu	 [291]	30 days	
Passengers must have a medical certificate proving they do not have COVID-19
X

 Samoa	 [202]	60 days		X

 Albania	 [11][12]	90 days		X

 Armenia	 [23][24]	90 days	
90 days within any 180 day period
Holders of passports for public affairs do not require a visa for up to 30 days.[25]
√

 Bosnia and Herzegovina	 [52]	90 days	
90 days within any 180 day period[53]
√

 Ecuador	 [88]	90 days	
90 days within any one-year period
√

 Mauritius	 [176]	90 days		√

 Morocco	 [186]	90 days		X

 Tunisia	 [269]	90 days[270]		X

 San Marino	 [227][228]	90 days.[229]	
Although officially no visa is required, at least a Single Entry Schengen visa is required to enter San Marino since it does not have an own airport facility and no border controls.
√


参考：
https://en.wikipedia.org/wiki/Visa_requirements_for_Chinese_citizens#Exit_and_Entry_Permit


With so many countries and talents, why can the English-speaking nation be at
the peak of civilization?


Author: Daniel Hannan


When I was four years old, a group of thugs broke into the family farm.
There is also a back door to the farm, and a path leads to the mountains.
My mother led me to escape and said to me: 
"Let's play the game! If you want to come back,
you must quietly..." 
My father is very calm, 
and he is responsible to the guys on the farm.
He said that he would never let a group of gangsters from the city drive him off his land.
I remember he was afflicting a 
recurrent infectious disease peculiar to whites in tropical regions.
He was sitting there in his nightgown and loaded the revolver with his thin, paper-like hand.

This is Peru under General Juan Velasco.He launched the coup in 1968 and messed
up the country.Only recently has Peru recovered from the chaos.In that coup,
Velasco nationalized important industries, issued a series of land reform laws,
and distributed all farms to his military friends.The police at the rudder are
naturally unwilling to protect personal property.

Naturally, my dad knew that the authorities were unreliable. He and the two
farm guards shot the bastards who were setting fire to the front door and
wanted to rush in, and drove them away.The danger finally passed.However, not
everyone is so lucky.All over the country, land was seized or confiscated, and
mines and fishing boats were forcibly confiscated.Foreign investment has
withdrawn, and multinational companies have recalled their employees, The
Anglo-Peru communities that were quite large when I was born have all
disappeared.

It was not until many years later that I was shocked to discover that no one
cared about all this at the time.In South America,People silently accepted that
property safety was not guaranteed, the law became a dead letter, and the
people elected 政府 The distant status quo.What you have may be robbed at any
time, sometimes without even a decent reason.Regimes are constantly changing,
and the constitution is frequently revised and changed.But at the same time,
South Americans, like those who have migrated overseas, never think such things
will be talked about English Country happened.Wait till i grow up 英国 After
studying and returning to Peru during the holidays, I began to realize the huge
contrast between the two countries.

After all, Peru is also a Western country in name.It belongs to christianity 文
明 Body, its founders regarded themselves as followers of the Enlightenment,
insisting on rationality, 科学 , Democracy and civil rights.However, Peru and
other pull 美国 Like home, it has never reached the height of a society under
the rule of law that naturally exists in North America.The north and south
areas were colonized almost at the same time. mainland , Live off like a set of
control experiments.North America was colonized by the British, who brought
with them faith in property rights, personal freedom, and representative
government.South America was colonized by the Iberians, who copied large
estates and semi-feudal society from Spain.Despite being poorer in natural
resources than her southern neighbors, North America has become the most ideal
living area in the world, attracting trillions of people who dream of
freedom.In contrast, South America still maintains an almost primitive state of
darkness before the democratically elected government as described by
Hobbes.Legal rule never comes from primitive physical violence, regardless of
whether this power adopts the method of mobilizing the masses or controlling
force.

Given this sharp distinction, it is incredible to deny the difference between
two different cultures.But don't misunderstand me.I am a loyal Spanish fan.I
love Spanish literature, history, drama and music.I have had a great time in
every Latin American country and 17 of the 16 autonomous regions in
Spain.Needless to say, I love Spanish culture.However, the longer I stay there,
the harder it is for me to believe that the "English world" and the "Spanish
world" will belong to the same Western civilization.

After all, what is "Western civilization"?In the opening quotation, what is the
meaning of Churchill?There are three indispensable elements in what he said.

First, the principle of the rule of law.Modern governments have no right to
make rules. Rules exist in a higher dimension and are interpreted by
independent arbitration institutions.In other words, the law is not a tool for
the government to control the country, but an operating mechanism to ensure
that any individual seeks relief.

Second, personal freedom.The freedom to say whatever you want, the freedom to
hold meetings with fellow seekers, the freedom to do business without
hindrance, the freedom to dispose of personal assets, the freedom to choose
jobs and employers, and the freedom to hire and fire people.

Third, representative government.Without the consent of the legislators, laws
cannot be promulgated, nor can taxes be levied; these legislators should be
elected by the people and accountable to the people.Now, readers may wish to
ask themselves (think about the past XNUMX years) how many countries that are
customarily labeled as "Western countries" adhere to the above principles?How
many countries still adhere to these principles firmly today?

After I was elected as a member of the European Parliament in 1999, this
question has been lingering in my mind.The premise of the establishment of the
EU is that the 28 member states belong to the same civilized community.In
theory, even though there will be differences in the cultures of different
countries, all members have signed a commitment to share Western liberal and
democratic values.

But the reality is not the case.The rule of law, democratic government, and
individual freedom constitute the three principles of Western civilization.
Their status in European countries is different.When EU member states take
collective action, these three principles will be placed under the political
needs of each country at any time.The elites in Brussels set aside the
principles of the rule of law as long as they feel in the way.

Let me cite a recent example: the bailout in the euro zone is clearly illegal.
Article 125 of the Constitutional Treaty of the European Union clearly states:
“The Union shall not provide guarantees to the central governments, regions and
other public institutions of member states, other institutions governed by
public laws, and public utilities.” This clause is not just a technical one. It
is stipulated that it is based on Germany's agreement to stop the circulation
of the mark as a prerequisite.Therefore, Merkel said: "Under this treaty, we
cannot do any rescue operations."

However, when everyone discovered that if there is no cash injection into the euro, 
it will not be guaranteed, the provisions of the treaty were immediately
thrown aside.
Christine Lagarde, then French Finance Minister and now President of the International Monetary Fund, cheered for everything they did, saying:
"We violated all regulations because we want to close the bank and take action to save the euro zone." 
The Lisbon Treaty is very clear, but it cannot save the market!"

In the eyes of the British, this action is nondescript.The rules have been
formulated in clear language that lawyers can use, but when it gets in the way,
the terms are "evaporated."When the British media reported the incident in this
way, it attracted cynicism such as "island state mentality" and
"Anglo-Saxon-style dead brains."As a Portuguese member of the European
Parliament said to me, everyone else believes that "practical effects are more
important than legislation."

Democracy is the same.It is regarded as a means to achieve an end, although
everyone yearns for it, but it ends. The EU Constitutional Treaty, later called
the Lisbon Treaty, was repeatedly rejected in referendums in various countries:
in 2005, 55% of the French and 62% of the Dutch rejected it; in 2008, 53% of
the Irish again Voted against.Europe’s response is to ignore it, continue to
implement the treaty, and complain that English-speaking countries do not
understand Europe.

As for the idea that individuals should be as free as possible without the
coercion of the state, it is considered the stubborn opinion of the Anglo
circle through and through.The EU continues to extend its power to new areas:
legislation determines which vitamins we can buy, how much security deposits
the bank needs to hold, when we go to get off work, and how should herbal
treatments be regulated... Every time, I ask: "In the end Are there any special
problems that need to be formulated to solve new regulations?" The answer is
always: "Old Europe doesn't care!" It seems that the lack of regulation in
everything is anti-natural, although it may be exactly the natural state of
things that should be.In continental Europe, the meanings of the terms "not yet
regulated" and "illegal" are very similar.

These places where English is the first language are collectively referred to
as the "Anglo-Saxon World" in Europe.This title is not based on race but on
culture.When the French say "lesanglo-saxons" or the Spanish say "los
anglosajones", they are not referring to the descendants of Anglo-Saxons, but
people who speak English and identify with small government, whether they are
in San Francisco or Slygo. Or Singapore.

Many in continental Europe Comment According to the reader, the United States,
the United Kingdom, 澳大利亚 They form an "Anglo-Saxon" civilization with
people from other English-speaking countries, and their biggest feature is that
they all believe in the free market.For some American readers, this may be a
bit surprising.I personally feel that American friends tend to treat the United
Kingdom and other European places as one, and emphasize the exceptions in their
own history.However, as we have seen, few people from other countries view the
United States that way. In the early 1830s, Tocqueville visited the United
States.He is often cited as a witness to American exceptionalism.

However, on the first page of "On American Democracy", he pointed out that one
of the themes of the book is that the English-speaking countries brought their
unique political and cultural concepts to the New World and took root in the
New World. This process is completely different from that. The American
colonies of France and Spain.He wrote: "Americans are autonomous British." This
sentence is often quoted, but it certainly has not spread widely.

In the past XNUMX years of international conflicts, 
this free continent has defended its values three times.
In the two world wars and the subsequent Cold War, 
the country that placed the individual above the power of the state defeated the opposite country.
In these three conflicts, how many countries have always stood firmly on the side of freedom?
The list is short but it includes most democracies where English is the first language.

Readers may have objections: 
Wouldn't this stand in line simply be categorized by ethnicity and language? !
Because the United Kingdom is caught in the flames of war, 
all English-speaking countries in the world naturally sympathize with their home country.
This is indeed a partial explanation. 
A few hours after Britain declared war on September 1939, 9, 
New Zealand Labor Prime Minister
Michael Joseph Savach said in his hospital bed, 
“With gratitude for the past and confidence in the future, 
we stand with Britain without fear. Together.
Where does she go, where do we follow; 
where does she stand, where do we stand".
Whenever I think of this scene, I can't help but tears in my eyes.
But this is not the full explanation.
Readers can look at the distribution of World War II monuments 
outside of Europe and count the number of volunteers.
In World War II, New Zealand mobilized a total of 3  people, 
South Africa 21.5 , Australia 41, Canada 99.5 million, 
and India 106 million. 
Most of them were voluntarily enlisted.

What power is calling these young people to cross half of the earth, 
just like calling their parents in the First World War, 
to fight for a country they might
never have seen with their own eyes?Is it just a connection between blood and
language? !Are these two world wars just an racial conflict, an enlarged
version? 南斯拉夫 Split or the Hutu massacre of the Tutsi? !

All this does not depend on the government mobilizing soldiers to the
battlefield, nor does it depend on people responding to the call
immediately.Soldiers rarely indulge in emotional matters.But in their diaries
and correspondence, we will find that they have a firm fighting spirit that
they are fighting to defend a way of life that is superior to the enemy.In both
world wars, they believed that they were in " Fight for freedom ", just like
the slogan of that era.

In 1915, the radical newspaper West India wrote: “Most West Indians are
descendants of slaves. Today, they are fighting for human freedom alongside
their brothers in their motherland.” In the same year, Sergeant Sergeant Hiram
Singh wrote to his Indian family in the damp trenches of northern France. The
letter said: "We must honor those who have given us salt. Our political system
is good and noble."

There is also a Maori leader who said in 1918 when he recalled the tribesmen in
the German colony, “We know the Samoans and they are our relatives. We know the
East and West Africans in Germany, and we also know Herero How did people
become extinct. This is enough. For 78 years,We are not being ruled by the
British, but integrating their governance into our own.Experience tells us that
the British regime is based on the eternal principles of freedom, equality and
justice. "

We usually think that today’s universal values ​​will win the final victory
sooner or later.However, nothing is necessary to win.If the outcome of the
Second World War is different, freedom is likely to be driven back to the North
American continent.If the Cold War took a different approach, then the two
camps might end up together.To be honest, the victory of the West is a series
of military successes achieved by the "English-speaking nation".

Of course, such speaking lacks diplomatic skills, so writers and politicians
are more willing to use the term "Western" rather than "Anglo circle."However,
what exactly is meant by "Western"?In World War II, this name refers to the
country that fought against Nazi Germany.During the long years of the Cold War,
"Western" refers to NATO (NATO) member states and their allies on other
continents.

With the fall of the Berlin Wall, the meaning of "Western" was quickly
refreshed.Samuel Huntington divided the world into a broad cultural circle in a
speech in 1992 and subsequent works.He summarized his views as " Clash of
civilizations ",and prophecy (For now, it is not very accurate) The conflicts
between cultural circles, not within cultural circles, will become more and
more intense.The West found by Huntington originated from the split between the
Latin and Greek branches of Christianity, and this religious split occurred in
1054.According to Huntington's classification, "Western" is composed of
European countries that are culturally Catholic or Protestant rather than
Eastern Orthodox, as well as countries such as the United States, Canada,
Australia, and New Zealand.

This definition is closely related to the Western military framework.Of course,
as far as the status quo of the aforementioned countries is concerned, this big
framework is also in constant change.Some countries that are now part of the
NATO group are still living in their memories, either belonging to Hitler or
under orders Stalin , Or both.In fact, outside of the English-speaking world,
count those countries that have historically continued to have representative
government and freedom under the rule of law. This number is so small that it
can only be counted as Switzerland, the Netherlands, and the Nordic countries.

Just like Mark Steyn has always been rough and not rough, he said that
continental Europe has contributed to the world exquisite oil paintings,
melodious symphonies, French wine, Italian actresses and, if not, are enough to
make us fascinated by diversity. The various things of culture.But when we
examine the process of forming the concept of the "politicized West"
characterized by loyalty to liberal democracy, it is not difficult to find that
this period of history looks more like elemental culture and political
centralization (even though it may have implemented a democratic government). ,
Instead of having a common sovereign government by the states like the United
States.All political leaders in Portugal, Spain, and Greece spent their
childhoods in despotism, as did Chirac and Merkel.We have forgotten how few
peaceful constitutional reforms are in this world and very few have occurred
outside the Anglo circle.

The border of ideology advances much faster than the border of the
country.European countries fully embrace the wave of Western values. The first
wave occurred after 1945, and the second wave occurred in 1989.Using the term
"Western Value" in this context is actually quite polite.What we really mean is
that these countries have accepted the basic characteristics of the
Anglo-American political system.

Election of parliament, habeas corpus, freedom of contract, equality before the
law, open markets, freedom of the press, freedom to change religious beliefs,
jury system... all of the above cannot be said to be born of an advanced
society. Components, they are the unique product of political ideology
developed with the help of the English language.This ideology, together with
this language, spread so widely that we often forget that their origin is
actually unique.

Take an example of clothing as an analogy.Herbert George Wells once asserted
that the British are one of the few countries in the world who do not have a
national dress.He was wrong about this.Suits and ties are the British national
clothes, but now there is no "Britishness" at all, they are commonplace all
over the world.Men in most countries dress like British in formal occasions,
but wear American jeans at other times.

Of course, there is still no shortage of defensive positions.Occasionally you
can see Bavarian men in their lederhosen and women in tight-waisted wide
skirts.Some Arabs still wrap themselves in robes and scarves.But in general,
the Anglo circle has lost its unique appearance.All this thanks to the
industrial revolution; of course, the revolution in textile fabrics is the
first to take the lead.Throughout the 20th century, English-speaking countries
used their images to weave cloth for the world, and in the process, they seemed
to forget that global clothing is actually their own clothing.

When we mention a country, we often involuntarily focus on the country's most
unusual highlights, rather than those special products that have been
successfully exported.For example, when asked about the most famous British
meal, people tend to say "steak and patties" instead of "sandwiches."When it
comes to the British national football, the answer is cricket, not football.It
is no exception when it comes to values.Speaking of how to define the
characteristics of the British political system, foreigners or Britons will
almost unanimously talk about the monarchy, the House of Lords, the hammer,
temple wigs, and other symbolic clothing used in parliamentary
procedures.Similarly, if this question is replaced by asking about the
political characteristics of the United States, the answer may be the sky-high
cost of election campaigns, corporate donations with ulterior motives, and
insidious offensive advertising.In fact, the last two examples failed to truly
capture the greatest feature of the two countries, that is, the lawmakers are
responsible to everyone, and the change of government is the result of peaceful
universal suffrage.

The rule of law is less than we think, and suppression and centralization are
more common.People are born to be competitive animals. As long as the
environment permits, they are always arbitrary and willful.Politically, a
medieval European monarch is no different from a modern African government
dictator.Once people have the right to make rules, how can they not manipulate
the rules according to their own likes and dislikes? !They will obey the drive
of instinct and develop a system that allows their offspring to maintain the
superior privileges.Monopoly power, inheritance of status, and institutional
special funding for the ruling class. These rules once spread all over the
world, but they are still common today.The real question is not whether liberal
democracy can win at the end, but how it can start from the beginning.

We are still affected by that epoch-making event.The inhabitants of the damp
and cold island country on the western end of Eurasia accidentally established
the notion that the government must obey the law and have no alternative.The
rule of law ensured the safety of property and contracts, which in turn gave
birth to industrialization and modernity Capitalism . in human history For the
first time, a system that rewards production as a whole rather than supports
plundering has been formed for the first time.Facts have proved that this
system is highly adaptable.It is carried by English speakers, all the way
across the ocean, or with the help of colonial rulers, or through the voluntary
practice of loyal colonists, in the ancient parliament hall of Philadelphia, it
finally crystallized into the U.S. Constitution in a subtle way. .

This example is so successful that almost all countries in the world today,
without exception, want to copy this example, or at least want to clone its
shell.Even the shameless dictatorship now has a so-called Congress.The
trembling delegates are gathering together in the so-called political party
organization to meet the president's will.Even the dirtiest authoritarian
regime has its own Supreme Court, at least on paper, it is no longer a tool of
power.But truly meaningful political freedom—freedom under the rule of law in
representative democracies—is still a rare phenomenon.It is a mistake to wish
wishfully that this system will live longer than the hegemony of the
English-speaking nation.

The Anglosphere is not so much a national concept as a cultural concept, and
this is the greatest source of her influence.The Victorian writers tried their
best to prove the superiority of the English nation in terms of race. Their
evidence was controversial at the time, but it is even harder to gain a
foothold today.Living in Melbourne The children of the Greek couple are richer
and more free than his cousin on Mytilene. The reason is not about race, but
about the political system.

Part of the problem lies in the ambiguity of the terminology itself.The Anglo
circle is a new word recently invented. The Oxford English Dictionary explains
the Anglo circle like this: “a group of countries whose main native language is
English.” However, the American writer James Bennett's definition is more
accurate, which made the term popular:

To become a member of the Anglo circle, you need to follow the basic customs
and values ​​that form the core of English culture. They include personal
freedom, the rule of law, emphasis on contracts and contracts, and freedom is
the first pursuit of political and cultural values.The countries that make up
the Anglo circle share common historical memories: the Magna Carta, the Bill of
Rights of the United Kingdom and the United States, the jury system, the
presumption of innocence, and common law principles such as "one person's house
is his castle".

So, which countries are included in the Anglo circle?The five core countries
that meet all definitions are Australia, Canada, New Zealand, the United
Kingdom, and the United States.Most definitions also include Ireland.In
addition, the islands of Singapore and former British colonies, such as Bermuda
and the Falkland Islands, are also included in this circle by most
definitions.Some definitions of the Anglo circle also include the democratic
countries on the Caribbean coast and South Africa.If the "elephant"-that is,
India (this image was once quite popular)-are also included, then the
"elephant" will account for two-thirds of the population of the Anglosphere.

There was no controversy about the view that the spread of freedom and the
process of the rise of the Anglo circle as one.After the Reformation, many
English-speakers regarded the superiority of their civilization as the help of
heaven.Their civilization is to build a new Israel, a country chosen by God and
mandated to bring freedom to the world.Royal navy anthem "Rule, Britannia!" "
(Rule Britannia!) deserves to be a hymn of British freedom: "When Britain was
placed on the blue sea by the Creator at the beginning of the world..." We sang
this song in unison so many times that we stopped. I thought about it less, and
the same belief, in a stronger form, inspired the first North Americans.

Religious passion is decimated day by day, but the faith in the vocation is
still determined.British and American historians have found their ancestors’
trails on the road to modernization and greatness: the formation of the common
law, the Magna Carta, the Great Admonishment, the British Bill of Rights of
1689, the U.S. Constitution, and the technological revolution. , Abolish
slavery.

Since the 20th century, the patriotism that preached Anglo-American history has
become obsolete. Marx Confucianism, anti-colonialism, and multiculturalism
became popular in chronicling onto the stage.Those historians who sang the
praises of Anglo-American political milestones became the target of public
criticism. They were denounced as cultural arrogances, and closed their doors
to congratulate themselves.The most unfortunate thing is that they are outdated
and swept into the garbage dump of history.

For Jefferson and his followers, a Whig person not only pursues manhood,
independence, and republican spirit, he also has a unique identity closely
linked to the ancient British cause. A widely circulated pamphlet published in
1775 defined the behavior of patriots as abiding by "the principles of the
Whigs from before the Glorious Revolution of 1688 to the time."

What are these principles?The pamphlet provides a simple and clear list: the
person who makes the law must be directly generated from the ballot box and be
responsible for it; the executive branch is controlled by the legislature;
there is no public consent and legal authorization, no taxation; everyone is
free from arbitrary Punishment, personal property must not be confiscated;
decision-making must be made as much as possible for the people affected by
this; power must be dispersed; no one, even the king, can be above the law;
property safety is guaranteed; disputes must be in an independent place Judge
rulings; protect freedom of speech, religion, and assembly.

Whether in the United Kingdom or the United States, those who support the above
principles have reason to call themselves "patriots."The problems they
discovered were completely unaware of the problems they had discovered: the
freedom that they cherished was largely restricted to the English-speaking
world, while the opponents in their home countries wanted to pull the political
system into an autocratic one. In the foreign model.

The opposition has won steadily. This is the tragedy of our time.After the
English-speaking nations formed and exported the most successful government
system in human history, they became daunted by their own achievements.The
intellectual elite in Britain viewed the value of the Anglo circle as a
stumbling block to integrating into European political integration.Their
Australian allies believe that Anglo values ​​are the centrifugal force of
their country’s return to Asia policy.In the United States, especially the
current government, the Anglo circle ideology is nothing more than the legacy
of the "white dead" of European males, a hangover that the colonialists do not
want to wake up.The multicultural pattern in every English-speaking country
prevents schools from teaching children that they are the heirs of a single
political heritage.

Up to now, most Anglo countries are gradually abandoning the "Whig Party
principle before the Glorious Revolution": the promulgation of the law no
longer needs to be passed by Congress, and it can be done by adopting an
executive order: taxation can also be done without the consent of the people. ,
The bank rescues the market can be secretive: the change of power at the local,
provincial, and national levels is now completed only in the capital; the
representative elected by the election in the past is replaced by the
administrative permanent establishment; the government expenditure is so high
that the people previously thought it was long enough to trigger a rebellion.
degree.If we want to know why the power of the Anglo circle is declining, we
really don't need to look more.

What makes the English-speaking nation rise is not because of the magical
characteristics of their genes, not because of their fertility and fertility,
nor their military technological advantages, but their political and legal
systems.

Regardless of whether people are willing to admit, the well-being of mankind
depends on the existence and success of these systems.As a loose alliance of
nations, the Anglo circle should continue to exert its affinity for driving
force in this century.Without this impetus, the future will undoubtedly look
darker and colder.

  • Let's talk about current affairs: the Chinese Communist Party pushes for
    the implementation of the Internet文明Building, Xi thought will "lock the
    throat" to the Internet?
  • Let's talk about current affairs: the CCP promotes the implementation of
    the Internet文明Building, Xi thought will "lock the throat" to the
    Internet?
  • A mysterious tribe who has mastered science 5000 years ago, can judge the
    changes of time, comparable to Maya文明
  • The Sumerians’ knowledge of cosmic objects is beyond our imagination, and
    they have ancient aliens文明, Maybe they are aliens
  • 文明How to defeat barbarism

—— "The Gene of Freedom"

<!--
 vim: set nu fdm=marker ft=markdown tw=65 wrap:
-->

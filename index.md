---
Title: 历史笔记前言做 Index page based on Preface of "History Notebook"
Date:  2021-11-20
layout: home
Category: History
---


[English Version: ](./y10m/notebook.history.preface.en)

<pre>

天堂的门洞比针眼更小，
大象却可以走进去，
然而污秽的人爬不入去。

地狱的门洞是相反的，
人们蜂拥着爬出来。

在这一份历史笔记中，研究的重点是地狱。

是什么养料培育着繁荣的地狱生态？

秽史养育了污秽的人。

地狱的史书充满谎言，
不可以反驳，只能复述，
不可以研究，只可以宣传...
不可以证伪，只能证明，
暴力加持着秽史成为永远的正确，流下黄泉。

被秽史污染的人沦为地狱生物，不得解脱。

为中共或者其它独裁者篡改历史的人，
那些故意污染人类史的人，
销售这些史书的人，
讲授这些课程的人，
传播这些谎言的人，十亿人，
你们和你们的后代如何才能解脱厄运？

中共的历史可以允当地狱的修造手册，
我们将在更长久的历史中研究地狱的宏观结构：金字塔。

如何修造地狱？

首先，建立权力的金字塔，
金字塔是地狱的基本几何结构，
根据几何学原理，它至少具备如下功能：

  - 混淆逻辑，让人类不辨真伪。是非不分，则善恶颠倒。
  - 倒置美丑，让人类无法建立人体美学，失去人性的艺术创造力，相反以食人为美。
  - 替代神学，反复的推动畜生们爬到塔顶冒充伪神。
  - 残害人体，升华凌辱和性侵害的乐趣，以构陷、酷刑和死亡构建权力台阶。
  - 葬送自由，埋葬言论自由，从而扼杀学术自由和信仰自由。
  - 扭曲科学，把科学技术变为囚禁人类的牢笼，师夷长技以制夷。
  - 洗脑愚民，让奴隶们真诚的拥护奴隶制。
  - 反向法制，培育愚民，再以法制民，放纵禽兽，牧羊牧民，立食民之法。
  - 退化语言，比如，给吃人的禽兽们起一个理想的名字：祖国。
  - ...

人类在权力的金字塔上必须爬，
食欲、性欲、生存欲都一步步转化为向上爬的欲望，
这样的生物自然的可以被称为金字塔上的_爬虫_，好好学习，天天向上。
爬虫们胜利的污染人性，造成大量人类的死亡。
相比环境污染，人性的污染更为令人恐惧，
大地将变为废土，中国变为 SODOM, 地球就变成地狱。
金字塔是制造这一切的充分必要条件，
这是几何学，难于否认，没有岐义，
不需要废话来否定之否定。

塔上没有言论自由也就没有思想自由，
没有学术和信仰的自由，
这里的审美缺乏艺术的想像力，
这里没有创造力除了谎言和暴力，在这里，
忠孝礼义、仁义道德、温良恭俭... 都成了假货。

所谓悠久的历史变为极权的恶性循环，朝代迭代，堕落退化。

集权的金字塔是实心的宝塔，
中国的金字塔不是埃及人修的，
今天在塔顶的是中共，
这是一群最爱国的  人，
它们不知道自己爱的其实是金字塔。

这些爬虫们远逝的祖先也不知道有一个叫做中国的祖国可以把他们的后人变成这样。
十几亿人不断的变形为爬虫，堆积到唯一的顶点，
爬虫之塔不断蠕动，却始终维持金字塔的形状。

这就是金字塔超越时空的力量，它来自地狱，
它就是地狱的构造蓝图，它是地狱微生物的基因。

先辈们叹息：世无英雄乃使竖子成名。
祖先被奴役了，
才让祖国篡改出伟大的秽史，
秽史养育污秽的人，
良知变成狡猾，
舍是为了得，
糊涂变得难得，
可是人们还是变成了愚民，
不可抗拒的，甚至在知识渊博的愚民们心里，
自古以来就不知道正义和邪恶早成为了一对同义词。

拆不掉金字塔，就算是逃出地狱的  人，
不仅浑身散发着臭气，而且也早晚要被拖回去。

当伪神和佛道儒家都跪在金字塔台阶上之后，
在秽史中，人们就忘记了什么是人？

是人民还是愚民？

是爬虫？还是地狱军团？
是 RNA 还是 DNA 中的指令？
让一群群毒虫撕咬着堆积成为一座魔法塔，
金字塔是无敌的结构，屹立千年，再千年。

起义者的热血再次从台阶上流淌而下，
浇铸为更罪恶的地基，生灵涂炭，
而大血祭正是金字塔的月经。

高科技的囚笼内，监控更强，
可是，生依然带来死。
熙熙攘攘，攘攘熙熙，
你想要如何？你行你上。

金字塔是无敌的，
它无法单一的从内部或者外部被拆除，
几乎没有人能够支付拆除它的成本。

然而不断降低的知识成本却已经让中国的金字塔被浸泡在人类知识的海水中了，
虽然它还在残酷杀人，在加力镇压。再看看吧，
它把活摘人体器官发展成为医学产业，造福人类。
它把互联网发展成防火墙，保护愚民。

拆除金字塔的方法之一*可能是*尝试自己研究自己真实的历史。
每个中国人血脉的历史都超过 TA 的祖国的。
祖国这个词是祖先的后人的不肖子孙发明的。
镇压人类的极权政府的历史相比每个人的来源不成比例的虚弱而且浅薄。

我们将在历史中寻找神坛的遗迹，
以人类史为逃离地狱的指引，
以史料为材料重修教堂，
伸展双臂分担十字架上的痛苦。
在重写的历史中澄清真相、恢复常识，找回逻辑、重构美学，
希望由此能够保全人体、仰望天堂。
为此也必然要重新理解生命，
回顾生与死，接受罪与罚，
由此分享到信仰，
在科学知识和宗教伦理中祈祷自由和救赎。

在金字塔眼中，
我们是万恶的逃亡者。
可是人类史就是逃亡史，
这里就是逃亡者重新写的历史学习笔记。

中共、五毛、小粉红恶灵退散。

本书是以「[人类史和中国](history.and.china)」为基础改写的，
编辑工作刚开始，主要目的：
一是为了精简，让中学生在更短的时间内，比如一周内甚至一天内可以读完。
从而让一个有着基础阅读能力的中国人可以在人类史的背景中更直接的面对这个问题：什么是人？
二是我们将中国(东亚地缘)简化为金字塔，以几何方法面对被中文扭曲的历史。
三是为了探索在2020年中共瘟疫（武汉肺炎，COVID-19）重置全世界之后金字塔的拆解方法。

历史不是写出来的，
每个人心中的历史要点也有不同取舍，
这里只是提供一个例子。
请注意人类史本身应该有足够的空间容纳不断增加而且对立的两种逻辑力量：证明与证伪。
以科学的态度来看，这两种人类的力量都是可以穿越时空的，而且还仅仅只是思维的力量。
希望一千亿人的生命连接成的历史应该有可能展示足够的力量来拯救以十亿计数的中国人，
使其得以免除被镇压在金字塔下承受退化为爬虫类的命运。

逃亡者的最高荣誉就是为拆解地狱奉献了哪怕微小的一份力量，
我们是人类，我们是同类，
逃离地狱的路径和拆解地狱的方法须要公开而且安全的分享，
让这些知识可以被研究，被测试，被证明和证伪，
逐渐提纯为知识中纯净的良知。



</pre>


## 正文

[历史笔记](./y10m/notebook.history)

什么时候算是古时候？

如果以物理学中大爆炸的理论做为宇宙的来源，
那就是接近 138 亿年前。

那时候没有人，
没有天，没有地，
没有万里山河。

那么请问什么是人？

人类的思想能不能探索到那个时候，什么都没有的时候？

我们就是正在做这样的探索...
